'use strict';

const config = require('./config');

const fs = require('fs');

const BrowserStack = require("browserstack");

class BrowserStackImageLoader {
    constructor(credentials = config.browserStackCredentials) {
        this.credentials = credentials;
        this.screenshotClient = BrowserStack.createScreenshotClient(this.credentials);
    }

    async getBrowsers() {
        return new Promise((resolve, reject) => {
            this.screenshotClient.getBrowsers((error, browsers) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(browsers);
                }
            });
        })
    }

    async generateScreenshots(options) {
        return new Promise((resolve, reject) => {
            this.screenshotClient.generateScreenshots(options, (error, job) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(job);
                }
            });
        })
    }

    async getJob(id) {
        return new Promise((resolve, reject) => {
            this.screenshotClient.getJob(id, (error, job) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(job);
                }
            })
        })
    }
}

const imageLoader = new BrowserStackImageLoader();

const dumpAllBrowsers = async () => {
    if (process.argv.includes('--dump_browsers')) {
        const fileName = 'allBrowsers.json';
        console.log('Dumping all browsers in file', fileName);
        const allBrowsers = await imageLoader.getBrowsers();
        const json = JSON.stringify(allBrowsers, null, '\t');
        return new Promise((resolve, reject) => {
            fs.writeFile(fileName, json, 'utf8', (err) => {
                if (err) {
                    reject(err)
                } else {
                    resolve();
                }
            });
        })

    }
};

// https://www.npmjs.com/package/browserstack#screenshotclientgeneratescreenshotsoptions-callback
const createOptions = (siteUrl, options, browsers = config.browsers) => {
    options.url = siteUrl;
    options.browsers = browsers;
    return options;
};

const delay = async (value = 5000) => {
    return new Promise(resolve => {
        console.log('Delay', value);
        setTimeout(resolve, value);
    })
};

const parseSite = async (siteObject) => {
    const siteUrl = siteObject.url;
    console.log('Parsing site', siteUrl);
    const options = createOptions(siteUrl, siteObject.options);
    console.log('Creating task...');
    let task = await imageLoader.generateScreenshots(options);
    console.log('Task', task.job_id, 'was created');
    let job = await imageLoader.getJob(task.job_id);
    while (true) {
        const state = job.state.toLowerCase();
        console.log('Task', task.job_id, 'has state', state);
        if (state === 'pending' || state === 'queue' || state === 'queued_all') {
            await delay();
            job = await imageLoader.getJob(task.job_id);
        } else if (state === 'done') {
            console.log('Task', task.job_id, 'has finished.');
            const {screenshots} = job;
            console.log('Received', screenshots.length, 'screenshots.');
            const result = [];
            for (let screenshot of screenshots) {
                result.push(screenshot.image_url);
            }
            return result;
        } else {
            throw 'Unknown state: ' + state;
        }
    }
};

const Trello = require("trello");

const { api_key, token } = config.trello.credentials;
const trello = new Trello(api_key, token);
// https://customer.io/actions/trello/
const idList = config.trello.idList;

const generateDescription = (siteUrl, browserObject, imgUrl) => {
    let result = `**URL tested:** ${siteUrl}\n`;

    result += `**Properties**\n`;
    result += `\n`;
    result += `- **Device:** ${browserObject.device}\n`;
    result += `- **Browser:** ${browserObject.browser} ${browserObject.browser_version}\n`;
    result += `- **Operating System:** ${browserObject.os} ${browserObject.os_version}\n`;
    result += `- **Resolution:** ${browserObject.resolution}\n`;
    result += `- **Screen size:** ${browserObject.screen_size}\n`;
    result += `- **Viewport:** ${browserObject.viewport}\n`;
    result += `- **Aspect Ratio:** ${browserObject.aspect_ratio}\n`;
    result += `\n`;

    result += `**Screenshot Attached**\n`;
    result += `[Screenshot URL](${imgUrl})\n`;

    result += `**[Click here]**`;

    const params = new URLSearchParams({
        os: browserObject.os,
        os_version: browserObject.os_version,
        device: browserObject.device,
        device_browser: browserObject.browser,
        zoom_to_fit: true,
        full_screen: true,
        url: siteUrl,
        speed: 1,
        host_ports: 'google.com,80,0',
        start_element: 'ClickedReproduceIssueFromtrello',
        start: true,
        furl: siteUrl
    });

    result += `(https://live.browserstack.com/dashboard#${params.toString()}) **to reproduce the issue on Browserstack**\n`;

    return result;
};

const processSites = async (sitesArr = config.sites) => {
    for (let siteObject of sitesArr) {
        const arrScreenshotsUrl = await parseSite(siteObject);
        for (let i = 0; i < arrScreenshotsUrl.length; ++i) {
            const link = arrScreenshotsUrl[i];
            const browserObject = config.browsers[i];
            const description = generateDescription(siteObject.url, browserObject, link);
            const linkUrl = new URL(link);
            const pathname = linkUrl.pathname;
            const arr = pathname.split('/');
            const name = arr[arr.length - 1];
            const card = await trello.addCard(name, description, idList);
            /*const attachment =*/ await trello.addAttachmentToCard(card.id, link);
        }
    }
};

const run = async () => {
    console.log('Script started.');
    await dumpAllBrowsers();
    await processSites();
    console.log('Script finished.');
};

run();

module.exports = imageLoader;
