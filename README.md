## Preparation

### Create configuration file

Get idList https://customer.io/actions/trello/

Create file ```config.json``` with

```json
{
  "browserStackCredentials": {
    "username": "user name",
    "password": "password"
  },
  "sites": [
    {
      "url": "\"https://google.com\"",
      "options": {
        "wait_time": 20
      }
    }
  ],
  "browsers": [
    {
      "os": "android",
      "os_version": "4.1",
      "browser": "Android Browser",
      "device": "Google Nexus 7",
      "browser_version": null,
      "real_mobile": false,
      "resolution": "resolution",
      "screen_size:": "screen_size",
      "viewport": "viewport",
      "aspect_ratio": "aspect_ratio"
    }
  ],
  "trello": {
    "credentials": {
      "api_key": "api key",
      "token": "token"
    },
    "idList": "5c7.................f0b3c"
  }
}
```


### Install dependencies

Run command ```npm install```

## Running app

Run command ```npm start```